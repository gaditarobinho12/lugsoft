<div class="row">
        <div class="col-md-3">
         
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><?=_lang('notify','text_title_notify')?></h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
              <li class="s"><a href="<?=base_url(); ?>notify/"><i class="fa fa-envelope-o"></i><?= _lang('notify','text_all');?></a></li>
                 </li>
                <li class="<?= isset(ci()->notify->news)?'active':''; ?>"><a href="<?=base_url() ?>notify/news"><i class="fa fa-envelope"></i> <?= _lang('notify','text_news');?>
                  <span class="label label-warning pull-right"><?= ci()->notify_resume->total;?></span></a></li>
                <li class="<?= isset(ci()->notify->opens)?'active':''; ?>"><a href="<?=base_url() ?>notify/open"><i class="fa fa-envelope-o"></i><?= _lang('notify','text_open');?></a></li>
                 </li>
               
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid hide">
            <div class="box-header with-border">
              <h3 class="box-title">Labels</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <?php
        if(!isset(ci()->read)){
        ?>
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= _lang('notify','text_visualization'); ?></h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <div class="pull-right">
                 
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                    <?php 

                    if(!count(ci()->list)){
                        ?>
                         <tr>
                             <td>
                                <div class="text-center text-warning">
                                <?= _lang('notify','text_no_item'); ?>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }else{
                    
                    foreach(ci()->list as $list){?>
                    <tr>
                      
                        <td class="mailbox-subject">
                        
                        <a href="<?= base_url("/notify/open/$list->id") ?>"> 
                        <?php if($list->open ==10){ ?>
                            <?= $list->title; ?> - <?= $list->text; ?>
                        <?php }else{
                            ?>
                         <b><?= $list->title; ?> - <?= $list->text; ?></b> 
                            <?php

                        } ?> 
                      
                        
                </a>
                    </td>
                        
                        <td class="mailbox-date"><?php echo ago($list->date_create);?> </td>
                        <td>
                        <?php if($list->open ==10){ ?>
                        <i class="fa fa-unlock"></i>  
                        <?php }else{
                            ?>
                        <i class="fa fa-lock"></i>  
                            <?php

                        } ?> 
                        </td>
                    </tr>
                    <?php }
                    }
                    ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
             
                <div class="pull-right">
                
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>

   <!-- /.col -->
        <?php
        }else{

            ?>

<div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= ci()->list->title;?></h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              
           
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
               

                <p><?= ci()->list->text;?></p>

              </div>
              <!-- /.mailbox-read-message -->
            </div>
           
            <!-- /.box-footer -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
            <?php
        }
        ?>
     
      </div>