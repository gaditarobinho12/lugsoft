<?php 

class Modules extends MY_Controller{


    function __construct(){
       
        
        $this->load_modules();
       
    }

    function load_modules(){

        if(is_auth_page('install')){

            return false;
        }
        $this->ci = &get_instance();

        /*
        $text  = $this->ci->load->library('Translate');
        var_dump($this->ci->translate->run('pt','en',' frase'));

        $this->ci->lang->load('pt','portuguese');

      */
        $this->ci->load->library('modules_model');

        $list = glob(__DIR__.'/modules/*/module_*.php');

        foreach($list as $file){
         
            include_once $file;
            
            $base_name = basename($file);
            $base_name = str_replace('.php','',$base_name);
            $dir_name = dirname($file);
            

            $module = str_replace(['module_'],'',$base_name);
            
            //Model
            $dir_model = $dir_name.'/model.php';
            $model = new stdClass();
            
            if(file_exists($dir_model)){
                include_once $dir_model;
                $model = "{$base_name}_model";
                $model = new $model();
                $model->core = $this->ci;
                $model->code = $module;
                $model->type = 'module';
            }
            //Controller
            $dir_controller = $dir_name.'/controller.php';
         
            $controller = new stdClass();
            
            if(file_exists($dir_controller)){
                include_once $dir_controller;
                $controller = "{$base_name}_controller";
                $controller = new $controller();
            }

            if(strpos(file_get_contents($file),'function __construct') == false){
                echo "$module required method __construct";
                exit;
            }
            
            if($this->is_enable($module)){
             
                $this->load[$module] = new $base_name();
                $this->load[$module]->model = $model;
                $this->load[$module]->code = $module;
                $this->load[$module]->type = 'module';
                $this->load[$module]->controller = $controller;

                $this->load[$module]->core = $this->ci;

                $this->load[$module]->lang = $this->lang($module); 

                if(method_exists($this->load[$module],'on')){
                    $this->load[$module]->ons = $this->load[$module]->on();
                }
                if(method_exists($this->load[$module],'autoload')){
                    if(!defined('autoload_module_'.$module)){
                        define('autoload_module_'.$module,1);
                        if(!(count($this->ci->uri->segments) and $this->ci->uri->segments[1] == 'auth')){
                            $this->load[$module]->autoload();
                        }
                    }
                } 
            }
           
        }
    }

    function load_module($module){

    }
    function _load($module,$status=10){

        $this->ci = &get_instance();

        $this->ci->load->library('modules_model');

        if(!isset($this->load[$module])){

            $file = __DIR__."/modules/{$module}/module_{$module}.php";
            if(file_exists($file)){
                include_once $file;
                
                $module = "module_{$module}";
                $this->load[$module] = new  $module();
              
                $this->load[$module]->lang = $this->lang($module); 
            }else{
                return false;
            }
            
        }

        return $this->load[$module];
    }

    
    function lang($mod,$key=null){

        $this->ci = &get_instance();
        $name = 'portugues';


       $file_lang = __DIR__.'/modules/'.$mod.'/lang/'.$name.'/lang.php';
       
        if(file_exists($file_lang)){
            include $file_lang;
            $this->ci->_lang[$mod] = $lang;
            return $this->ci->_lang[$mod];
        }else{
            $file_lang = __DIR__.'/modules/'.$mod.'/lang/english/lang.php';
            if(file_exists($file_lang)){
                include $file_lang;
                $this->ci->_lang[$mod] = $lang;
                return $this->ci->_lang[$mod];
            }
           
        }
       
        return[];

    }
    function is_enable($module,$status=10){
       
        $module = str_replace('module_','',$module);
        $result = $this->ci->modules_model->status($module);
        
        if(isset($result->status)){
            return $result->status >= $status?true:false;
        }
        
        $this->ci->modules_model->install($module,$this->_load($module)->about);
        return true;
    }

    function ons($module,$method,$parans){
     
        if(!isset($this->load)){
            $this->load_modules();
            
        }

        if(isset($this->load) and isset($this->load[$module]) ){
            
            foreach($this->load as $mod){
                
                if(isset($mod->ons)){
                    
                    if(isset($mod->ons[$module]) and isset($mod->ons[$module][$method])){
                        $mod->ons[$module][$method]($parans);
                    }
                }
            }
        }
    }
    
    function struct($data=[]){
        $data['lang'] = $this->lang($this->code);
        return [
            '_contents_'=>view_struct('struct/modules/'.$this->code.'/view',$data,true)
        ];
    }
    function __call($method,$p){
      
        if(isset($this->code)){
            $module = $this->_load($this->code);
            $module->{$method}();
        }
      
       
        //redirect('/');
    }


    function data($p1,$p2){

    }
}