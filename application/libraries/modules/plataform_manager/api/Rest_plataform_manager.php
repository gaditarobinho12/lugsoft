<?php 


class Rest_plataform_manager extends Rest{
  var $param_in_method  = false;
    function __construct()
    {

      parent::__construct();
    }



    function required(){

        if($this->method == 'POST' or $this->method == 'PUT'){
         
          $this->add_required([
            'email'=>[
              'force'=>true,
              'format'=>'cpf_cpnj'
            ]
          ]);
        }

        if($this->method=='PUT'){
          $this->add_required([
            'cpf_cnpj'=>[
              'force'=>true,
              'format'=>'cpf_cpnj'
            ]
          ]);
        }
        if($this->method=='GET'){


        }
    }

    function _put(){

    }
    function _purge($data){


    }

    function _get($data){
     
        $this->add_data($data);
        $this->_print($data);
        if(isset($data->plataform_manager) and $data->plataform_manager == 'install_plataform'){

            $this->install_plataform();
        }
    }

    function install_plataform($plataform="5aeb3f6886354"){

        $this->plataform = $plataform;
        $this->host = '5aeb3f6886354.lsoft.local';
        $prefix_db = 'lug_';
        ci()->load->dbforge();
      
        $plataform_sql = APPPATH.'db/'.$plataform.'.sql';

        copy(APPPATH.'db/example.sql',$plataform_sql);
        $this->filereplace($plataform_sql);

        $host_db = APPPATH.'db/'.$this->host.'.php';
        copy(APPPATH.'db/example._php',$host_db);


        
        $this->filereplace($host_db);

        if (ci()->dbforge->create_database($prefix_db.$plataform))
        {
            redirect("/install/db/$plataform");
                echo 'Database created!';
            //    $this->execute(APPPATH.'db/'.$plataform.'.sql');
                exit;
        }

        
    }

    function install_db(){

    }


    function execute($file){
            ci()->load->dbforge('db_default', TRUE);
            //(ci()->load->dbforge())
        // Set line to collect lines that wrap
            $templine = '';

            // Read in entire file
            $lines = file($file); 

            // Loop through each line
            foreach ($lines as $line)
            {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '')
            continue;

            // Add this line to the current templine we are creating
            $templine .= $line;

            // If it has a semicolon at the end, it's the end of the query so can process this templine
            if (substr(trim($line), -1, 1) == ';')
            {
            // Perform the query
            ci()->db->query($templine);

            // Reset temp variable to empty
            $templine = '';
            }
            }
    }

    function filereplace($file){
        $content = file_get_contents($file);
        $content = $this->_replace($content);
        file_put_contents($file,$content);
    }
    function _replace($string){
        
        $plataform = $this->plataform;
        $userid = 1;

        $string = str_replace('{plataform}',$plataform,$string);
        $string = str_replace('{userid}',$userid,$string);

        return  $string;
    }
   

}