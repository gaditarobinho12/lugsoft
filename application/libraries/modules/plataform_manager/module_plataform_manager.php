<?php 

class module_plataform_manager extends Modules{
    
    function __construct(){

        $this->about = [
            'code'=>'plataform_manager',
            'name'=> $this->lang('plataform_manager')['name'],
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];

        $this->ci = & get_instance();
    }

    

    function autoload(){

        if(isset($this->ci->userid) and !isset($_SESSION['notify_plataform_manager'])){

            $_SESSION['notify_plataform_manager'] = true;
            $this->ons('notify','Ok',[

            ]);

        }

        if(is_auth_page('plataform_manager')){
            if(!isset($_SESSION['userId'])){
                redirect('/');   
            }
        }
        

        $is_active = $this->is_active();
        ci()->plataform_manager_is_active = $is_active;
        ci()->plataform_manager_basic_complement = $this->basic_complement();

        
        if(is_auth_page()){

            if($is_active or !$this->model->configs()){

                redirect('/'.$this->code);
            }

           
        }

        
    }

    function view(){
      
        $ci = & get_instance();
        view('_default_/',['struct'=>$this->struct(['ci'=>$ci]),'ci'=>$ci]);
    }

    function is_active(){
        $host = $_SERVER['SERVER_NAME'];
        $host = $_SERVER['SERVER_NAME'];
        
        $this->load_modules();
        $manager = $this->_load('plataform_manager',10);
        if($manager->model->is_active($host)){
           
            return true;
        }
        
    }
    function on(){
        return [
            'notify'=>[
                'Ok'=>function($data){

                    if($this->model->notify(0)){
                        return true;
                    }
                    if(!isset($this->ci->modules)){
                        $this->ci->load->library('modules');
                    }
                    
                    $notify = $this->ci->modules;

                    if(isset($notify->load['notify'])){
                        if(!$this->model->check_product_group('plataform_manager')){
                            $notify->load['notify']->model->insert([
                                'title'=>_lang('plataform_manager','text_title_notify'),
                                'text'=>_lang('plataform_manager','text_text_notify'),
                            ]);
                        } 
                    }
                   
                   
                }
            ]
        ];
    }
    function basic_complement(){
        
    }

    
}