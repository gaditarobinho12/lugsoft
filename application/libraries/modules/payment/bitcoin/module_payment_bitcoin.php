<?php 


class module_payment_bitcoin extends  Modules{
    function __construct(){
        $this->about = [
            'code'=>'payment_bitcoin',
            'name'=> 'Payment bitcoin',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = & get_instance();

        $this->limit_time = time()+(60*15);
    }

    function view(){

        if(isset($_POST['signature'])){
           echo $product_id = $_POST['product_id'];
        }
    }



    function address($product_id){
        $get_address = $this->get_address($product_id);
        if($get_address){
           
            return $get_address;
        }

        $ci = ci()->db;
        $ci->insert('ext_payment_bitcoin_addresses',[
            'product_id'=>$product_id,
            'address'=>md5(rand(0,10000000))
        ]);

     //   $ci->address =
        
    }

    function get_address($product_id){
        $db = ci()->db;
       // $db->where('product_id',$product_id);
        $db = $db->get('ext_payment_bitcoin_addresses');

        if(count($db->row()) > 0){
            return $db->row()->address;
        }
    }
    function payment_signature($product_id){

    }

    function create_invoice($address){
      
        if(!$this->get_invoice($address)){
            $this->core->create_invoice([
                'type'=>'bitcoin',
                'type_product'=>'signature',
                'key_reference'=>$address,
                'amount'=>1,
                'date_limit'=>$this->limit_time
            ]);
        }
        
    }

    function get_invoice($address){
        $invoice = ci()->db;
        $invoice->where('key_reference',$address);
        $invoice->where('date_limit > '.time());
        $invoice=$invoice->get('ext_payment_invoices');
        return $invoice->row();
    }
}