<?php 

if(isset(ci()->invoices)){
    if(!has_permission('invoices')){
        redirect('/permission/invoices');
    }else{


        if(!isset(ci()->edit)){
        ?>


                <div class="">
                    <div class="row">
                        
                        
                        <div class="col-md-12">
                        <h4>Bootstrap Snipp for Datatable</h4>
                        <div class="table-responsive">

                                
                            <table id="mytable" class="table table-bordred table-striped">
                                
                                <thead>
                                
                                <th>First Name</th>
                                    <th>type payment</th>
                                    
                                    
                                    <th class="text-right"></th>
                                </thead>
                    <tbody>
                    
                    <?php foreach($this->invoices as $inv):?>
                    <tr>
                    <td>Mohsin</td>
                    <td><?= $inv->type;?></td>
                
                    <td>
                    <p data-placement="top" data-toggle="tooltip" title="Edit" class="pull-right" style="margin-right:3px;">
                    
                        <a class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal"  href="<?= base_url()?>payment/invoices/edit/<?= $inv->id;?>" ><span class="glyphicon glyphicon-pencil"></span></a>
                    
                    </p>
                    <p data-placement="top" data-toggle="tooltip" title="Delete"  class="pull-right" style="margin-right:3px;"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
                    </tr>
                    
                    <?php endforeach;?>
                    
                
                    
                    </tbody>
                        
                </table>

                <div class="clearfix"></div>
                <ul class="pagination pull-right">
                <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                </ul>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>



        <?php

        }else{//é edicao

            ?>



<form method="POST">
    <select name="status">
    <option value="0">Pendente</option>
        <option value="-1">Cancelado</option>
       
        <option value="10">Concluido</option>
    </select>

    <button name="change_status">Mudar Status</button>

</form>



            <?php
        }
    }
}else
if(isset(ci()->payment)){

    ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript" src="https://blockchain.info/Resources/js/pay-now-button.js"></script>
<div style="font-size:16px;margin:0 auto;width:300px" class="blockchain-btn"
     data-address="<?= ci()->address;?>"
     data-shared="false">
    <div class="blockchain stage-begin">
        <img src="https://blockchain.info/Resources/buttons/donate_64.png"/>
    </div>
    <div class="blockchain stage-loading" style="text-align:center">
        <img src="https://blockchain.info/Resources/loading-large.gif"/>
    </div>
    <div class="blockchain stage-ready">
         <p align="center">Please Donate To Bitcoin Address: <b>[[address]]</b></p>
         <p align="center" class="qr-code"></p>
    </div>
    <div class="blockchain stage-paid">
         Donation of <b>[[value]] BTC</b> Received. Thank You.
    </div>
    <div class="blockchain stage-error">
        <font color="red">[[error]]</font>
    </div>
</div>

<script>

$(function(){
    $('.blockchain-btn').trigger('click');
})
</script>
    <?php
}else{


    ?>


<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<div class="">
	<table id="cart" class="table table-hover table-condensed">
    				<thead>
						<tr>
							<th style="width:50%">Product</th>
							<th style="width:10%">Price</th>
							<th style="width:8%">Quantity</th>
							<th style="width:22%" class="text-center">Subtotal</th>
							<th style="width:10%"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-th="Product">
								<div class="row">
									<div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
									<div class="col-sm-10">
										<h4 class="nomargin">Product 1</h4>
										<p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
									</div>
								</div>
							</td>
							<td data-th="Price">$1.99</td>
							<td data-th="Quantity">
								<input type="number" class="form-control text-center" value="1">
							</td>
							<td data-th="Subtotal" class="text-center">1.99</td>
							<td class="actions" data-th="">
								<button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
								<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>								
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr class="visible-xs">
							<td class="text-center"><strong>Total 1.99</strong></td>
						</tr>
						<tr>
							<td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
							<td colspan="2" class="hidden-xs"></td>
							<td class="hidden-xs text-center"><strong>Total $1.99</strong></td>
							<td>
                            <form action="<?= base_url();?>payment" method="POST">
                            <input name="payment" value="bitcoin" >
                            <input name="signature" value="<?=ci()->product_id; ?>" >
                            
                            <button name="product_id" value="<?=ci()->product_id; ?>" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></button>
                            </form>
                            </td>
						</tr>
					</tfoot>
				</table>
</div>
    <?php
}

?>