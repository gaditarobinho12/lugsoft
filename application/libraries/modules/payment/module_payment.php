<?php 


class module_payment extends  Modules{
    function __construct(){
        $this->about = [
            'code'=>'payment',
            'name'=> 'Payment',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = & get_instance();
    }

    function view(){
        $ci = & get_instance();
      
      

        $product_id = $_POST['product_id'];
        ci()->product_id = $product_id;

        if(isset($_POST['payment'])){
           
            $_SESSION['HTTP_REFERER'] = base_url();

            $_payment = $_POST['payment'];
            ci()->payment = $_payment;

            include_once __DIR__."/bitcoin/module_payment_bitcoin.php";
            
            $payment = new module_payment_bitcoin();
            $payment->core = $this;
            ci()->address = $payment->address($product_id);

            if($payment->create_invoice(ci()->address)){

            }
            view('_default_/',['struct'=>$this->struct(['ci'=>$ci]),'ci'=>$ci]);

        }else
        if(isset($_POST['signature'])){
            if(count($_POST)){
                $_SESSION['HTTP_REFERER'] = $_SERVER['HTTP_REFERER'];
    
            }
           
            $payments = $this->getpayments($product_id);
            if(!count($payments)){
                $this->ci->session->set_flashdata('error', 'Sorry,
                no payment method implemented!');
                redirect($_SESSION['HTTP_REFERER']);

                
            }else{
                $this->payments =$payments;
              
                view('_default_/',['struct'=>$this->struct(['ci'=>$ci]),'ci'=>$ci]);
            }


          
        }else{
            echo "Payments";
            exit;
            redirect($_SESSION['HTTP_REFERER']);
        }
        
    }

    function getpayments($product_id){
        return[
            'bitcoin'
        ];
    }
    function payment_signature($product_id){

    }

    function create_invoice($data){
        ci()->db->insert('ext_payment_invoices',$data);
    }

    function invoices($p=[]){
        $inv_id = null;
        $ci = ci();
        $seg = $ci->uri->segments;
        $seg = (Array) $seg;
        if(in_array('edit',$seg)){
            $ci->edit = $seg[4];
            $inv_id = $seg[4];

            if(isset($_POST['change_status'])){
                $status = $_POST['status'];
                $this->change_status($inv_id,$status);
            }

        }

        $ci->invoices = $this->get_invoices($inv_id);

        view('_default_/',['struct'=>$this->struct()]);
    }
    function change_status($invoice_id,$status){
        $data_invoice = $this->get_invoices($invoice_id);
       
        if(count($data_invoice)){
            $data_invoice = $data_invoice[0];
            if($status != $data_invoice->status){
                $to_data_invoice = $data_invoice;
                $to_data_invoice->status = $status;
                $this->apply_change_status($data_invoice,$to_data_invoice);
            }
         
        }
    }

    function apply_change_status($data,$to){
        
        $status = ci()->db;
        $status->where('id',$to->id);
        $status->update('ext_payment_invoices',[
            'status'=>$to->status
        ]);


        if($to->status == 10){
            $this->payment_ok($to);
        }
    }


    function get_type_product(){

    }
    function payment_ok($to){
        $to = (Array) $to;
        $this->ons('payment','Ok',$to);
    }

    function get_invoices($invoice_id=null){
        $invoices = ci()->db;
        if($invoice_id!=null){
            $invoices->where('id',$invoice_id);
        }
        $invoices=$invoices->get('ext_payment_invoices');
        return $invoices->result();
    }
}