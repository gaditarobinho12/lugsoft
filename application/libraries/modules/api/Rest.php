<?php
/**
 * Created by PhpStorm.
 * User: Robson
 * Date: 12/12/2017
 * Time: 15:41
 */

class Rest extends module_api {
    var $param_in_method = false;

    function valida_cpf($cpf){
        for( $i = 0; $i < 10; $i++ ){
            if ( $cpf ==  str_repeat( $i , 11) or !preg_match("@^[0-9]{11}$@", $cpf ) or $cpf == "12345678909" )			return false;
            if ( $i < 9 ) $soma[]  = $cpf{$i} * ( 10 - $i );
            $soma2[] = $cpf{$i} * ( 11 - $i );
        }

        if(((array_sum($soma)% 11) < 2 ? 0 : 11 - ( array_sum($soma)  % 11 )) != $cpf{9})return false;
        return ((( array_sum($soma2)% 11 ) < 2 ? 0 : 11 - ( array_sum($soma2) % 11 )) != $cpf{10}) ? false : true;
    }

    function valida_cnpj( $cnpj ) {
        if( strlen( $cnpj ) <> 14 or !is_numeric( $cnpj ) ){
            return false;
        }

        $k = 6;
        $soma1 = "";
        $soma2 = "";

        for( $i = 0; $i < 13; $i++ ){
            $k = $k == 1 ? 9 : $k;
            $soma2 += ( $cnpj{$i} * $k );
            $k--;

            if($i < 12) {
                if($k == 1) {
                    $k = 9;
                    $soma1 += ( $cnpj{$i} * $k );
                    $k = 1;
                } else {
                    $soma1 += ( $cnpj{$i} * $k );
                }
            }
        }

        $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
        $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;

        return ( $cnpj{12} == $digito1 and $cnpj{13} == $digito2 );
    }

    function cpf_cpnj($value){

        $value = str_replace(['.','-','/'],'',$value);
        if($this->valida_cnpj($value) == false and $this->valida_cpf($value) == false){

            return [
                'format_error'=>"Formato invalido"
            ];
        }
    }



    function add_required($required){

        if(!isset($this->required)){
            $this->required = [];
        }

        $this->required = array_merge($this->required,$required);
    }

    function validate($type='data',$required=null){

        if(!isset($this->required) and $required==null){
            return;
        }

        if($type == 'data'){
            $required = $this->required;
            $data = $this->data;
        }
        if($type == 'parans'){
            $data = $this->parans;
        }

        $validade = $type=='data'?'validate_data':'validate_'.$type;

        foreach ($required as $key => $value) {

            if (isset($value['force']) and $value['force'] == true) {

                if (!isset($data->{$key})) {

                    $this->error[$validade][] = [
                        $key => isset($value["force_error"]) ? $value["force_error"] : 'required'
                    ];
                }
            }
            if (isset($data->{$key}) and isset($value['type']) and $value['type'] == 'int') {

                if (!is_numeric($data->{$key})) {

                    $this->error[$validade][] = [
                        $key => isset($value["type_error"]) ? $value["type_error"] : "required type numeric"
                    ];
                }

            }else
            if (isset($data->{$key}) and isset($value['format']) and $value['format'] != '') {

                $format = $this->rest->{$value['format']}($data->{$key});

                if($format!=false){

                    $this->error[$validade][] = [
                        $key => isset($format["format_error"]) ? $format["format_error"] : "(".$data->{$key}.") not is valid format"
                    ];
                }
            }
        }
        if($type!='data' and isset($this->error) and count($this->error) > 0){

            echo json_encode([
                'error'=>$this->error
            ]);
            exit;
        }
    }

    function add_data($data){
       if(!count($data)){
        $data = [];
       }
       if(!isset($this->_data)){
        $this->_data = [];
       }
        $this->_data =(Array) array_merge((Array)$this->_data,(Array)$data); 
    }

    function _print(){
        foreach($this->_data as $key => $values){
            if(is_numeric($key)){
                unset($this->_data[$key]);
            }
        }
        echo json_encode($this->_data);
    }

}
