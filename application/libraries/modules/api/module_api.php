<?php 

class module_api extends Modules{
    
    public $url = null;
    public $is_throw = false;

    public $key_encode = -1;
    public $valida_key = null;


    public $tags = [
        'cp','ddd','Chave_Acesso','Numero','Operadora','Usuario','Senha'
    ];

    public $Rota = null;

    public $dados = [];


    public $tagsModulo = [];


    public $Tipo_Requisicao = null;


    public $Url = null;

    public $retorno = null;


    public $retornar_resposta = false;

    function __construct(){

        $this->about = [
            'code'=>'api',
            'name'=> "Api to system",
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];

        $this->ci = & get_instance();

    }

    

    function autoload(){
        $this->ci = & get_instance();
       
        $code_plafaform = $this->_load('plataform_manager',10)->is_active();
      
        if(is_auth_page('api') and $code_plafaform == false){
            redirect('/plataform_manager');
            exit;
        }
        $this->data('ext_data',[
            'ext_type'=>'module',
            'ext_code'=>'plataform_data'
        ]);
    }

    
    function __call($name, $arguments)
    {
        $this->index($arguments);
    }


    public function index($args)
    {

        header("Content-Type: application/json");

        $metodo = $_SERVER['REQUEST_METHOD'];
        $recurso = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
        $recurso = $this->ci->uri->segments;
        $conteudo = file_get_contents('php://input');

        if($this->is_json($conteudo)){
            $conteudo = json_decode($conteudo);

        }elseif ($metodo == 'POST'){
            $conteudo = (Object) $_POST;
        }

        $list = new stdClass();
        $anterior = '';
        $get = '';
        $recurso = (Object) array_merge((Array)$recurso,$_GET);
        foreach ($recurso as $key => $value){

            if($anterior==''){
                $value = 'action';
            }
            if($anterior != ''){
                $list->$anterior = $value;
            }
            $anterior = $value;
            $get .= $value.",";
        }
        $recurso =(Object) $list;

      $file = __DIR__."/../{$recurso->action}/api/Rest_".strtolower($recurso->action).".php";

        include_once  __DIR__."/Rest.php";
    
        if(file_exists($file) and file_exists(__DIR__."/Rest.php")){

            include_once $file;

            $rest = str_replace('.php','',basename($file));
            $_class = $rest;
            $rest = new $rest();
            $rest->method = $metodo;
            $rest->data = $conteudo;
            $rest->parans = $recurso;
            $rest->rest = $rest;
            $rest->required();    
         
            $rest->validate();
            if(isset($rest->error) and count($rest->error) > 0){
                echo json_encode([
                    'error'=>$rest->error
                ]);
            }else{
              
                if(method_exists($_class,'_'.$metodo)){
                    $metodo = "_".$metodo;
                    $rest->{strtolower($metodo)}($recurso);
                }
             

            } 
            
            return;
            if(!function_exists($metodo)){

                header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
                die('{"erro": "Método não encontrado."}');
            }else{
                $metodo($recurso,$conteudo);
            }
        }else{
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
            die('{"erro": "Método não encontrado."}');
        }


        exit;

        return;
        switch ($metodo ) {
            case 'PUT':
                funcao_para_put($recurso, $conteudo);
                break;
            case 'POST':

                funcao_para_post($recurso, $conteudo);
                break;
            case 'GET':
                funcao_para_get($recurso, $conteudo);
                break;
            case 'HEAD':
                funcao_para_head($recurso, $conteudo);
                break;
            case 'DELETE':
                funcao_para_delete($recurso, $conteudo);
                break;
            case 'OPTIONS':
                funcao_para_options($recurso, $conteudo);
                break;
            default:
                header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
                die('{"msg": "Método não encontrado."}');
                break;
        }


    }
    function is_json($data){

        $this->is_str = false;
        $this->has = false;

        if(strlen($data) < 1){
            return;
        }

        $this->has = true;

        $str1 = substr($data,0,1);
        $str2 = substr($data,strlen($data)-1,1);

        $str3 = substr($data,0,1);
        $str4 = substr($data,strlen($data)-1,1);

        if($str1 == "{" and $str2 == "}"){

            return true;
        }if($str3 == "[" and $str4 == "]"){
            return true;
        }
        $this->is_str = true;

    }


    function view(){
      
        $ci = & get_instance();
        view('_default_/',['struct'=>$this->struct(['ci'=>$ci]),'ci'=>$ci]);
    }
}