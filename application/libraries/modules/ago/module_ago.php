<?php 

class module_ago extends Modules {

    function __construct(){
        $this->about = [
            'code'=>'ago',
            'name'=> 'Time ago',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = ci();
    }

    function ago($datetime, $full = false){

        if($datetime == '0000-00-00 00:00:00'){
            $date = '2018-05-04 20:00:00';
            return _lang('ago','default_value',$this->time_elapsed_string($date));
        }


        return $this->time_elapsed_string($datetime);
        return _lang('ago','default_value',1);
    }

    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' =>  _lang('ago','y_text'),
            'm' =>  _lang('ago','m_text'),
            'w' =>  _lang('ago','w_text'),
            'd' =>  _lang('ago','d_text'),
            'h' =>  _lang('ago','h_text'),
            'i' =>  _lang('ago','i_text'),
            's' =>  _lang('ago','s_text'),
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        $string ? implode(', ', $string) . ' ago' :  'just_now';

        if(!count($string)){
            return _lang('ago','just_now');
        }
        foreach($string as $key => $value){
            $lang = _lang('ago',$key,$value);
            break;
        }
        $lang = str_replace("êss",'êses',$lang);
        return $lang;
    }
}