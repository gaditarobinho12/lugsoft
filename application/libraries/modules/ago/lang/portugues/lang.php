<?php 


$lang['default_value'] = 'mais de %s';

$lang['just_now'] = 'Agora mesmo';

$lang['s'] = '%s atrás';
$lang['s_text'] = 'second';

$lang['i'] = '%s atrás';
$lang['i_text'] = 'minuto';

$lang['h'] = '%s atrás';
$lang['h_text'] = 'hora';

$lang['d'] = '%s atrás';
$lang['d_text'] = 'dia';

$lang['w'] = '%s atrás';
$lang['w_text'] = 'semana';

$lang['m'] = '%s atrás';
$lang['m_text'] = 'mês';

$lang['y'] = '%s atrás';
$lang['y_text'] = 'year';