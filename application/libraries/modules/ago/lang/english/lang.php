<?php 


$lang['default_value'] = 'More than %s';

$lang['just_now'] = 'just now';

$lang['s'] = '%s ago';
$lang['s_text'] = 'second';

$lang['i'] = '%s ago';
$lang['i_text'] = 'minute';

$lang['h'] = '%s ago';
$lang['h_text'] = 'hour';

$lang['d'] = '%s ago';
$lang['d_text'] = 'day';

$lang['w'] = '%s ago';
$lang['w_text'] = 'weeks';

$lang['m'] = '%s ago';
$lang['m_text'] = 'month';

$lang['y'] = '%s ago';
$lang['y_text'] = 'year';