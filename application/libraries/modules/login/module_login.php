<?php 

class module_login extends Modules{
    
    function __construct(){
        $this->about = [
            'code'=>'login',
            'name'=> 'Login in system',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0'
        ];

    }

    function views(){
        return[
            'form_top'=>['Recaptcha top','Acima do primeiro input padrão do formulario'],
            'form_bottom'=>['Recaptcha bottom','Abaixo input padrão do formulario'],
        ];
    }

    function onOk($parans){
        $this->ons('login','Ok',$parans);
    }
    function onValidate($data){
        $this->ons('login','Validate',$data);
    }
    function _ok(){
       $this->onOk(['username'=>'robson']);
      
    }
    function onLoad($parans=[]){

        $this->ons('login','Load',$parans);
    }
    function autoload(){
     // $this->onLoad();
    }
}