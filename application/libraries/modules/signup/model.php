<?php 


class module_signup_model {

    function createUserEmail($email,$password){
        $db = $this->core->db;
        
        $date = now();
        $db->insert('tbl_users',[
            'email'=>$email,
            'password'=>$password,
            'updatedDtm'=>$date,
            'createdDtm'=>$date,
        ]);
        
        return $db->insert_id();
    }
    
    function checkUserEmail($email){
        $db = $this->core->db;
        
        $date = now();
        $db->where([
            'email'=>$email
        ]);
        $db = $db->get('tbl_users');
        return $db->row();
    }
}