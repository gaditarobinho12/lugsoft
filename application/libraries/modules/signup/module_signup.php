<?php 

class module_signup extends Modules{
    

    
    function __construct(){
    
        $this->about = [
            'code'=>'signup',
            'name'=> 'Login in system',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];

    }

    function view(){
        
        if(isset($_POST['email'])){
            if($this->onValidate([])){
                unset($_SESSION['auth_email']);
                $email = $_POST['email'];
                $password = $_POST['password'];

               
                $user_id = $this->model->createUserEmail($email,getHashedPassword($password));
                if($user_id){
                    $_SESSION['userId']  = $user_id;
                    $data['userid'] = $user_id;
                    $data['email'] = $email;
                    $this->ons('signup','Ok',$data);
                }
                setFlashData('success', 'Você ja pode se logar no sistema!');
                 redirect('/loginMe');
            }else{
                setFlashData('error', 'This email in use!asd');
            }

            

        }
      
        $ci = & get_instance();
        view('_forms_/',['struct'=>$this->struct(['ci'=>$ci]),'ci'=>$ci]);
    }
    function error(){

    }


    function onValidate($data){
        $this->ons('signup','Validate',$data);
        $email = $_POST['email'];
      
        if(!$this->model->checkUserEmail($email)){
            return true;
        }
      
    }
   

}