<?php 

class module_products extends Modules{

    function __construct(){
        $this->about = [
            'code'=>'products',
            'name'=> 'Products',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = & get_instance();
    }


    function autoload(){
        global $module_products;
        $this->ci->signatures = [];
        $signatures = $this->model->type('signature');
        if($signatures){
            $this->ci->signatures = $signatures;
        }
    }
    

    
   
}