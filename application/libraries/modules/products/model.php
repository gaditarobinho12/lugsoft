<?php 


class module_products_model {


    function __construct(){

        $this->ci = ci();
    }


    function type($type){
        $db = $this->ci->db;
        $db->where('type',$type);
        $db = $db->get('ext_products');

        $types = [];

        foreach($db->result() as $sign){
            $plans = $this->plans($sign->id);
            if(count($plans)){
                $sign->plans = $plans;
                $sign->amount = $this->amount($sign->id);
                $types[] = $sign;
            } 
        }

        return $types;
       
    }

    function plans($sign_id){
        $db = $this->ci->db;
        $db->where('key','signature');
        $db->where('value',$sign_id);
        $db = $db->get('ext_products_data');

        $plans = [];
        foreach($db->result() as $plan){
            $plan = $this->data_plan($plan->product_id);
            if($plan){
                $plan->amount = $this->amount($plan->id);
                $plans[] = $plan;
            }
           
            
        }

        return $plans;
    }

    function data_plan($plan_id){
        $db = $this->ci->db;
        $db->where('id',$plan_id);
        $db = $db->get('ext_products');
        return $db->row();
    }

    function amount($id){
        $db = $this->ci->db;
        $db->where('product_id',$id);
        $db->where('key','amount');
        $db = $db->get('ext_products_data');
        $row =  $db->row();
        if($row){
            return $row->value;
        }
        return '0.0';
    }
}