<?php 


class module_notify extends Modules{


    function __construct(){
        $this->about = [
            'code'=>'notify',
            'name'=> 'Notifications',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = ci();
    }



    function autoload(){
       
        $seg = ci()->uri->segments;
        ci()->notify_resume = [];
        ci()->notify_resume = $this->model->get_result();


        if(isset($seg[2])){
            if($seg[2] == 'news' and !isset($seg[3])){
                ci()->list = $this->model->get_open(0)->list;
            }else
            if($seg[2] == 'open' and !isset($seg[3])){
                ci()->list = $this->model->get_open(10)->list;
           
            }elseif(isset($seg[3])){
                ci()->read = true;
                ci()->list = $this->model->get_open(null,$seg[3])->list;

            }
        }else{
            $list = $this->model->get_all();
            if($list and isset($list->list)){
                ci()->list = $list->list;
            }
            
        }
       
       
     
    }
    function get_resume(){

    }

    function view(){
        view('_default_/',['struct'=>$this->struct()]);
    }
    function open(){
        view('_default_/',['struct'=>$this->struct()]);
    }function news(){
        view('_default_/',['struct'=>$this->struct()]);
    }
}