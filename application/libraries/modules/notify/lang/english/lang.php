<?php 

$lang['text_not_resume_total'] = 'You have %s notifications';

$lang['text_view_all'] = 'View All';
$lang['text_not_resume'] = '<div class="text-center text-success">You have no new messages that have not been viewed!</div>';
$lang['text_title_notify'] = 'Notify';

$lang['text_no_item'] = 'No item to this filter';

$lang['text_news'] = 'News';
$lang['text_open'] = 'Open';
$lang['text_visualization'] = 'List view';
$lang['text_all'] = 'All';
?>