<?php 


class module_notify_model {

    function __construct(){
        $this->ci = ci();
        $this->db =   $this->ci->db; 
    }

    function insert($data){

        $notify = $this->db;
        $notify->insert('ext_notify_data',[
            'userid'=>$this->ci->userid,
            'open'=>0,
            'title'=>$data['title'],
            'text'=>$data['text'],
            'type'=>isset($data['type'])?$data['text']:'normal',
            'date_create'=>date('Y-m-d H:i:s')
        ]);
       
    }

    function open($id,$open=1){
        $notify = $this->db;
        $notify->where('id',$id);
        $notify->update('ext_notify_data',[
            'open'=>$open
        ]);
    }

    function get_result(){
        $data = new stdClass();

        if(!isset($this->ci->userid)){
            return $data;
        }
        $notify = $this->db;
        $notify->where('userid',$this->ci->userid);
        $notify->where('open',0);
        $notify = $notify->get('ext_notify_data');

        $data->total = count($notify->result());


        $notify = $this->db;
        $notify->where('userid',$this->ci->userid);
        $notify->where('open',0);
        $notify->limit(5);
        $notify = $notify->get('ext_notify_data');

        $data->list = $notify->result();
    
        return $data;
    }

    function get_news(){
        $data = new stdClass();

        if(!isset($this->ci->userid)){
            return $data;
        }
     
        $notify = $this->db;
        $notify->where('userid',$this->ci->userid);
        $notify->where('open',0);
        $notify->limit(20);
        $notify->order_by('id desc');
        $notify = $notify->get('ext_notify_data');

        $data->list = $notify->result();
    
        return $data;
    }
    function get_open($open=null,$id=false){
        $data = new stdClass();

        if(!isset($this->ci->userid)){
            return $data;
        }
     
        $notify = $this->db;
        $notify->where('userid',$this->ci->userid);
        if($open!=null ){
            $notify->where('open',$open);
          
        }elseif($open==0){
            $notify->where('open',0);
        }
        if($id!=false){
            $notify->where('id',$id);
            $this->open($id,10);
        }
        $notify->limit(20);
        $notify->order_by('id desc');
        $notify = $notify->get('ext_notify_data');

        if($id==false){
            $data->list = $notify->result();
        }else{
            $data->list = $notify->row();
        }
     
    
        return $data;
    }

    function get_all(){
        $data = new stdClass();

        if(!isset($this->ci->userid)){
            return $data;
        }
     
        $notify = $this->db;
        $notify->where('userid',$this->ci->userid);
       
        $notify->limit(2);
        $notify->order_by('id desc');
        $notify = $notify->get('ext_notify_data');

        $data->list = $notify->result();
     
    
        return $data;
    }
}
?>