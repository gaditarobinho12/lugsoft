<?php 

$notify = ci()->notify_resume;

?>
<li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-bell-o"></i>
              <?php if($notify->total){?>
              <span class="label label-warning"><?= $notify->total; ?></span>
              <?php }?>
            </a>
            <ul class="dropdown-menu">
                <?php if($notify->total){?>

                    <li class="header"><?= _lang('notify','text_not_resume_total',$notify->total);?></li>

                <?php }?>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  
                <?php foreach($notify->list as $not){?>
                    <li>
                    <a href="<?=base_url()?>notify/open/<?=$not->id;?>">
                      <i class="fa fa-warning fa-envelope text-warning"></i> 
                      <b><?=$not->title; ?>-</b>
                      <?=$not->text; ?>
                    </a>
                  </li>
                <?php }?>
                <?php 

                    if(!$notify->total){
                    ?>
                    <li class="header text-center"><br /><?=_lang('notify','text_not_resume')?><br /></li>
                    <?php
                    }
                
                ?>

                  
                </ul>
              </li>
              <li class="footer"><a href="<?=base_url()?>notify"><?=_lang('notify','text_view_all')?></a></li>
            </ul>
          </li>