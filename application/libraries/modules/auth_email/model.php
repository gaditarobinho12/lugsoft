<?php 

class module_auth_email_model {

    function data()
    {
        return [
            'limit_time'=>60*60000
        ];
    }
    function generate($data){

        $data_json = json_encode($data);

        $db = $this->core->db;
        
        $date = now();
        $db->insert('ext_data',[
            'user_id'=>$data['userid'],
            'ext_type'=>$this->type,
            'ext_code'=>$this->code,
            'date_update'=>$date,
            'date_create'=>$date,
            'data'=>$data_json,
            'reference'=>'code_'.$data['code']
        ]);

        return $this->get($data);
    }
    
    function delete($data){
        
        $db = $this->core->db;
        $db->where([
            
            'user_id'=>$data['userid'],
            'ext_type'=>$this->type,
            'ext_code'=>$this->code,
        ]);

        $db->delete('ext_data');
        
    }
    function get($data){
        $limit = date('Y-m-d H:i:s',strtotime(now())-$this->data()['limit_time']);
        $now = now();
        $db = $this->core->db;
        $db->where([
            'reference'=>'code_'.$data['code']  
        ]);
        $db->where("date_update BETWEEN '$limit' and '$now'");

        $db  =$db->get('ext_data');

        return $db->row();
    }
    function loginMe($userid)
    {
        $this->db = $this->core->db;
        $this->db->select('BaseTbl.userId, BaseTbl.password, BaseTbl.name, BaseTbl.roleId');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->where('BaseTbl.userId', $userid);
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->row();
        
        return $user;
    }
}
?>