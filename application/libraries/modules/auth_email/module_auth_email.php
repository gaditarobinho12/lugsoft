<?php 

class module_auth_email extends Modules{

    function __construct(){

        $this->about = [
            'name'=>'Autenticate by email',
            'code'=>'auth_email',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0'
        ];

    }

    function on(){
        return [
            'login'=>[
                'Ok'=>function($parans){
                    $parans['view']  = 'login';
                    $parans['code'] = md5('auth_email_'.time());
                    $parans['subject'] = 'Login in system'; 
                    $this->generate_code($parans);
exit;
                }
            ],'signup'=>[
                'Ok'=>function($parans){
                    $parans['view']  = 'signup';
                    $parans['code'] = md5('auth_email_'.time());
                    $parans['subject'] = 'Signup system';
                    $this->generate_code($parans);
exit;
                }
            ],
        ];
    }

    function generate_code($data){

        $verify_code  = rand(1000,9999);  



        include_once 'templates/'.$data['view'].'.php';

                    $data = array_merge($data,[
                        'userid'=>$data['userid'],
                        'subject'=>$data['subject'],
                        'email'=>$data['email'],
                        'text'=>$text,
                        'code'=>$data['code'],
                        'verify_code'=>$verify_code
                    ]);
                    $this->model->generate($data);

                    $this->send($data);
                   
    }

    function code($code){
        $data = $this->model->get([
            'code'=>$code
        ]);
        if(!isset($data)) {
            module_view('auth_email/login/auth_error_code', []);
        }else{
          
            $data = json_decode($data->data,1);
            if(isset($data['code'])){

                if(isset($_SESSION['userId']) and !isset($_SESSION['auth_email'])){
                    
                    $_SESSION['auth_email'] = $data['userid'];
                    $_SESSION['isLoggedIn'] = $data['userid'];
                    $this->model->delete($data);

                   redirect('/dashboard');
                }else
                if(isset($_POST['post_auth_email_code_verify'])){
                    $auth_email_code = $_POST['post_auth_email_code_verify'];
                    $code_verify = $_POST['code_verify'];
                  if($code_verify == $data['verify_code']){
                    $_SESSION['auth_email'] = $data['userid'];
                    $this->model->delete($data);
                    $this->loginMe($data);
                  }else{
                    $this->core->session->set_flashdata('error', 'Incorrect code, verify and try again!');
              
                    redirect('auth/auth_email/code/'.$code);
                  }
                }else{

                    module_view('auth_email/login/auth_code', $data);
                }
            }else{

                module_view('auth_email/login/auth_error_code', $data);
            }

        }
    }
    function loginMe($data){

        $result = $this->model->loginMe($data['userid']);

        if(!empty($result))
            {
                
                $sessionArray = array('userId'=>$result->userId,                    
                                        'role'=>$result->roleId,
                                        'roleText'=>$result->role,
                                        'name'=>$result->name,
                                        'lastLogin'=> now(),
                                        'isLoggedIn' => TRUE
                                );

                $this->core->session->set_userdata($sessionArray);

                $this->model->delete($data);
                redirect('/dashboard');

            }
    }
    function autoload(){
        $this->ci = & get_instance();


        
        if(is_auth_page()){

            if(isset($_POST['post_auth_email'])){
                if(isset($_SESSION['userId']) and !isset($_SESSION['auth_email'])){
                    if(!isset($_GET['auth_email'])) redirect('auth/auth_email/verify/auth_email?auth_email');
               
                }
                if(isset($_GET['auth_email']) and isset($_SESSION['auth_email'])){
                    redirect('/dashboard');
                }
            }elseif(isset($_SESSION['userId']) and !isset($_SESSION['auth_email'])){
                if(!isset($_GET['auth_email'])) redirect('auth/auth_email/verify/auth_email?auth_email');
           
            }
           
           
        }

        
    }

    function verify(){
        echo module_view('auth_email/login/auth_msg_secure', [],true);
    }
    function send($data){
    
   //     $this->generate_code($data);
        //   module_view('auth_email/login/auth', $data);
        $CI = setProtocol();        
     
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject($data['subject']);
        $CI->email->message(module_view('auth_email/login/auth', $data,TRUE));
        $CI->email->to($data["email"]);
        $status = $CI->email->send();


            echo module_view('auth_email/login/auth_msg_secure', [],true);
    }
    function reset($data){
        session_destroy();
        redirect('/loginMe');
    }
}