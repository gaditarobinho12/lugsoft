<?php 

class module_view extends Modules{
    
    function __construct(){
    
        $this->about = [
            'code'=>'view',
            'name'=> 'View for system'
        ];

    }

    function show($view,$data=[]){
		view('welcome_message',$data);
    }

}