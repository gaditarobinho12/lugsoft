<?php 


class module_affiliate extends Modules {

    function __construct(){
        $this->about = [
            'code'=>'affiliate',
            'name'=> 'Affiliates',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = ci();
    }

    function autoload(){
        
    }
}