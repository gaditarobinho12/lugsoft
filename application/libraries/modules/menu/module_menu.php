<?php 


class module_menu extends Modules {

    function __construct(){
        $this->about = [
            'code'=>'menu',
            'name'=> 'Menu',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = & get_instance();
    }

    function autoload(){
        getLang('menu_left');
        $this->ci->_lang['menu_left']['text'] = 'Text left';
    }
}