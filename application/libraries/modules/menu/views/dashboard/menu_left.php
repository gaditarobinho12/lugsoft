
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="#" >
                <i class="fa fa-plane"></i>
                <span>New Task</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#" >
                <i class="fa fa-ticket"></i>
                <span>My Tasks</span>
              </a>
            </li>
            

            <?php if(is_admin() or has_permission('menu')){?>
            <li class="treeview active">
              <a id="lnkReports" href="#">
                <i class="fa fa-files-o"></i>
                <span><?= _lang('menu','text_ext');?></span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu menu-open" style="display: block;">
                <li><a id="lnkEmployeeReport" href="http://feedbacker.codeinsect.com/employeeReport"><i class="fa fa-circle-o"></i> Employee Report</a></li>
                <li><a id="lnkSummaryReport" href="http://feedbacker.codeinsect.com/summaryReport"><i class="fa fa-circle-o"></i> Summary Report</a></li>
              </ul>
            </li>


            <?php }?>

            
          </ul>

          