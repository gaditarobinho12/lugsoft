<?php 

class module_session extends Modules{
    
    function __construct(){
    
        $this->about = [
            'code'=>'session',
            'name'=> 'Login in system'
        ];

    }

    function onOk($parans){
        $this->ons('login','Ok',$parans);
    }
    function _ok(){
        $this->onOk([
            'username'=>'robson'
        ]);
      
    }
}