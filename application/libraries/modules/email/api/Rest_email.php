<?php 


class Rest_email extends Rest{
  var $param_in_method  = false;
    function __construct()
    {

      parent::__construct();
    }



    function required(){

        if($this->method == 'POST' or $this->method == 'PUT'){
         
          $this->add_required([
            'email'=>[
              'force'=>true,
              'format'=>'cpf_cpnj'
            ]
          ]);
        }

        if($this->method=='PUT'){
          $this->add_required([
            'cpf_cnpj'=>[
              'force'=>true,
              'format'=>'cpf_cpnj'
            ]
          ]);
        }
        if($this->method=='GET'){


        }
    }

    function _put(){

    }
    function _purge($data){


    }

    function _get($data){

      $this->add_data($data);
      $this->_print();
    }
   

}