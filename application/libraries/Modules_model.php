<?php 

class Modules_model{

    function __construct(){
        $this->ci = & get_instance();
        $this->db = $this->ci->db;
        
    }
    function status($code){
        $module = $this->db;
        $module->where('code',$code);
        $module=$module->get('extensions');
        return $module->row();
    }

    function install($data,$data){
        $data = $data;
        $module = $this->db;

        $module->insert('extensions',$data);
    }

    function generate($data){

        $data_json = json_encode($data);

        $this->ci = & get_instance();

        $db = $this->ci->db;
        
        $date = now();
        $db->insert('ext_data',[
            'user_id'=>$data['userid'],
            'ext_type'=>$this->type,
            'ext_code'=>$this->code,
            'date_update'=>$date,
            'date_create'=>$date,
            'data'=>$data_json,
            'reference'=>$data['code']
        ]);

        return $db->insert_id();
    }
}

?>