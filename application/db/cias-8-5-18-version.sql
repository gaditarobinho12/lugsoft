-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 08-Maio-2018 às 14:48
-- Versão do servidor: 5.5.44-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `cias`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `extensions`
--

CREATE TABLE IF NOT EXISTS `extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(255) NOT NULL,
  `type` enum('module','pluguin') NOT NULL,
  `author` varchar(20) NOT NULL,
  `email_author` varchar(50) NOT NULL,
  `version` varchar(15) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135 ;

--
-- Extraindo dados da tabela `extensions`
--

INSERT INTO `extensions` (`id`, `name`, `code`, `type`, `author`, `email_author`, `version`, `status`) VALUES
(1, 'system for emails', 'email', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 0),
(2, 'Login in system', 'login', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(9, 'Autenticate by email', 'auth_email', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 0),
(10, 'Recaptcha Google', 'recaptcha', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 0),
(11, 'Login in system', 'session', 'module', '', '', '', 0),
(12, 'View for system', 'view', 'module', '', '', '', 0),
(14, 'Login in system', 'signup', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(120, 'Plataform manager', 'plataform_manager', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(121, 'Api to system', 'api', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(122, 'Products', 'products', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(123, 'Payment', 'payment', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(124, 'Menu', 'menu', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(126, 'Binario', 'binary', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(131, 'Time ago', 'ago', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(132, 'Notifications', 'notify', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(133, 'Affiliates', 'affiliate', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10),
(134, 'Language', 'language', 'module', 'Robson Gomes', 'gaditarobinho12@gmail.com', '1.0', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_data`
--

CREATE TABLE IF NOT EXISTS `ext_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_type` varchar(15) NOT NULL,
  `ext_code` varchar(30) NOT NULL,
  `data` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  `date_create` datetime NOT NULL,
  `reference` varchar(50) NOT NULL,
  `tip` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Extraindo dados da tabela `ext_data`
--

INSERT INTO `ext_data` (`id`, `ext_type`, `ext_code`, `data`, `user_id`, `date_update`, `date_create`, `reference`, `tip`) VALUES
(25, 'module', 'auth_email', '{"userid":1,"email":"gaditarobinho12@gmail.com","view":"signup","code":"9876f143c8ac9385500e4d0097aa9f6f","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 5657","verify_code":5657}', 1, '2018-04-20 06:02:04', '2018-04-20 06:02:04', 'code_9876f143c8ac9385500e4d0097aa9f6f', 0),
(26, 'module', 'auth_email', '{"userid":1,"email":"gaditarobinho12@gmail.com","view":"signup","code":"6f4c1411062dd1bf13a686268460b80f","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 8950","verify_code":8950}', 1, '2018-04-20 06:03:54', '2018-04-20 06:03:54', 'code_6f4c1411062dd1bf13a686268460b80f', 0),
(27, 'module', 'auth_email', '{"userid":12,"email":"gaditarobinho12@gmail.com","view":"signup","code":"df4f2419b9709c81011b3656f82aa60c","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 2150","verify_code":2150}', 12, '2018-04-20 06:15:58', '2018-04-20 06:15:58', 'code_df4f2419b9709c81011b3656f82aa60c', 0),
(28, 'module', 'auth_email', '{"userid":13,"email":"gaditarobinho12@gmail.com","view":"signup","code":"b0534b7972e206d30b328c8fabdc8a81","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 5784","verify_code":5784}', 13, '2018-04-20 06:16:30', '2018-04-20 06:16:30', 'code_b0534b7972e206d30b328c8fabdc8a81', 0),
(29, 'module', 'auth_email', '{"userid":14,"email":"gaditarobinho12@gmail.com","view":"signup","code":"c350fc5f8e133750e817af0ad4049d8c","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 7214","verify_code":7214}', 14, '2018-04-20 06:21:56', '2018-04-20 06:21:56', 'code_c350fc5f8e133750e817af0ad4049d8c', 0),
(30, 'module', 'auth_email', '{"userid":15,"email":"gaditarobinho12@gmail.com","view":"signup","code":"3179ff22f0a67d8fcb77051385cdb995","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 9711","verify_code":9711}', 15, '2018-04-20 06:24:24', '2018-04-20 06:24:24', 'code_3179ff22f0a67d8fcb77051385cdb995', 0),
(31, 'module', 'auth_email', '{"userid":16,"email":"gaditarobinho12@gmail.com","view":"signup","code":"5d099b231b873c03b3a1849e9201b2f2","subject":"Signup system","text":"<b>Registry in IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 1738","verify_code":1738}', 16, '2018-04-20 06:26:51', '2018-04-20 06:26:51', 'code_5d099b231b873c03b3a1849e9201b2f2', 0),
(34, 'module', 'auth_email', '{"email":"gaditarobinho12@gmail.com","userid":"18","view":"login","code":"5784b1c5762b829395b77c176dddf1db","subject":"Login in system","text":"<b>Visitor IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 9255","verify_code":9255}', 18, '2018-04-20 06:33:25', '2018-04-20 06:33:25', 'code_5784b1c5762b829395b77c176dddf1db', 0),
(35, 'module', 'auth_email', '{"email":"gaditarobinho12@gmail.com","userid":"18","view":"login","code":"9c8172f8800d2eed7f80aaffa88275a6","subject":"Login in system","text":"<b>Visitor IP address:<\\/b><br\\/>172.18.0.1<br\\/><b>Browser:<\\/b><br\\/>Chrome<br\\/><b>Location:<\\/b> Brazil,Esp&iacute;rito Santo,Vila Velha <br><br><b>Your Code:<\\/b> 3201","verify_code":3201}', 18, '2018-04-20 06:35:32', '2018-04-20 06:35:32', 'code_9c8172f8800d2eed7f80aaffa88275a6', 0),
(36, 'total_access', 'total_access', '1', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'localhost', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_language`
--

CREATE TABLE IF NOT EXISTS `ext_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(20) NOT NULL,
  `code_ext` varchar(20) NOT NULL,
  `key` varchar(20) NOT NULL,
  `value` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_notify_data`
--

CREATE TABLE IF NOT EXISTS `ext_notify_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `open` int(11) NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date_create` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `ext_notify_data`
--

INSERT INTO `ext_notify_data` (`id`, `userid`, `open`, `type`, `title`, `text`, `date_create`) VALUES
(6, 33, 10, 'normal', 'Settings Open', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-04 00:00:00'),
(9, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '0000-00-00 00:00:00'),
(10, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '0000-00-00 00:00:00'),
(11, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '0000-00-00 00:00:00'),
(12, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '0000-00-00 00:00:00'),
(13, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '0000-00-00 00:00:00'),
(14, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-04-03 00:00:00'),
(15, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-02 00:00:00'),
(16, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-03-05 00:00:00'),
(17, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-04 00:00:00'),
(18, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 00:17:45'),
(19, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 02:01:27'),
(20, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 13:43:16'),
(21, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 13:43:23'),
(22, 33, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 13:47:00'),
(23, 24, 10, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 15:13:45'),
(24, 24, 0, 'normal', 'Settings', 'You need to bind the signature in a group of products for  platform manager module to work correctly', '2018-05-05 16:48:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_payment_bitcoin_addresses`
--

CREATE TABLE IF NOT EXISTS `ext_payment_bitcoin_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `userid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `ext_payment_bitcoin_addresses`
--

INSERT INTO `ext_payment_bitcoin_addresses` (`id`, `address`, `userid`, `product_id`, `status`) VALUES
(2, '9af44fac846d300ad99003d0351d08c6', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_payment_invoices`
--

CREATE TABLE IF NOT EXISTS `ext_payment_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `type_product` varchar(30) NOT NULL,
  `payment_id` varchar(100) NOT NULL COMMENT '//Id principal do tipo de pagamento utilizado exemplo id primary gerado do na tabela do modulo do mercadopago',
  `key_reference` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `date_limit` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `ext_payment_invoices`
--

INSERT INTO `ext_payment_invoices` (`id`, `type`, `type_product`, `payment_id`, `key_reference`, `amount`, `date_limit`, `status`) VALUES
(6, 'bitcoin', '', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525393438, 10),
(7, 'bitcoin', '', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525394735, 0),
(9, 'bitcoin', 'signature', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525399619, 0),
(10, 'bitcoin', 'signature', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525444587, 10),
(11, 'bitcoin', 'signature', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525446424, 0),
(12, 'bitcoin', 'signature', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525448798, 10),
(13, 'bitcoin', 'signature', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525449706, 0),
(14, 'bitcoin', 'signature', '', '9af44fac846d300ad99003d0351d08c6', 1, 1525535110, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_plataform_manager`
--

CREATE TABLE IF NOT EXISTS `ext_plataform_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `userid` int(11) NOT NULL,
  `plataform` varchar(15) NOT NULL,
  `host_name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `ext_plataform_manager`
--

INSERT INTO `ext_plataform_manager` (`id`, `name`, `userid`, `plataform`, `host_name`, `active`) VALUES
(2, 'Oni', 24, '5aeb3f6886354', '5aeb3f6886354.lsoft.local', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_plataform_manager_data`
--

CREATE TABLE IF NOT EXISTS `ext_plataform_manager_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plataform_manager_id` int(11) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `ext_plataform_manager_data`
--

INSERT INTO `ext_plataform_manager_data` (`id`, `plataform_manager_id`, `key`, `value`, `status`) VALUES
(9, 2, 'url', '5aeb3f6886354.lsoft.local', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_products`
--

CREATE TABLE IF NOT EXISTS `ext_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('service','plan','signature','product') NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `plataform_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `ext_products`
--

INSERT INTO `ext_products` (`id`, `type`, `name`, `description`, `status`, `plataform_id`) VALUES
(1, 'signature', 'Primium1', 'Descrição premium', 1, 1),
(2, 'signature', 'Primium2', 'Descrição premium2', 1, 1),
(3, 'signature', 'Primium', 'Descrição premium', 1, 1),
(4, 'signature', 'Primium', 'Descrição premium', 1, 1),
(5, 'signature', 'Primium', 'Descrição premium', 1, 1),
(6, 'plan', 'Plano Basico', 'Descrição plano basico', 1, 1),
(7, 'plan', 'Plano Basico ', 'Adicional', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_products_data`
--

CREATE TABLE IF NOT EXISTS `ext_products_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `ext_products_data`
--

INSERT INTO `ext_products_data` (`id`, `product_id`, `key`, `value`, `status`) VALUES
(1, 1, 'amount', '10.0', 1),
(2, 6, 'signature', '1', 1),
(3, 6, 'amount', '9.0', 1),
(4, 7, 'signature', '1', 1),
(5, 7, 'amount', '2.1', 1),
(6, 2, 'signature', '100', 1),
(7, 7, 'signature', '2', 1),
(8, 2, 'amount', '100.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ext_products_group`
--

CREATE TABLE IF NOT EXISTS `ext_products_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identify` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `ext_products_group`
--

INSERT INTO `ext_products_group` (`id`, `identify`, `name`, `status`) VALUES
(1, 'plataform_manager1', 'Plataform Manager', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_last_login`
--

CREATE TABLE IF NOT EXISTS `tbl_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=302 ;

--
-- Extraindo dados da tabela `tbl_last_login`
--

INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(1, 2, '{"role":"2","roleText":"Manager","name":"Manager"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 10, '{"role":"2","roleText":"Manager","name":"Teste2"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Firefox 38.0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0', 'Linux', '0000-00-00 00:00:00'),
(18, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Firefox 38.0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0', 'Linux', '0000-00-00 00:00:00'),
(21, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(26, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(33, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(37, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(39, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(47, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(53, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(56, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(57, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(58, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(59, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(60, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(61, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(62, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(63, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(64, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(65, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(66, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(67, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(68, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(69, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(70, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(71, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(72, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(73, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(74, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(75, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(76, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(77, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(78, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(79, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(80, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(81, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(82, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(83, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(84, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(85, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(86, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(87, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(88, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(89, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(90, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(91, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(92, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(93, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(94, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(95, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(96, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(97, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(98, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(99, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(100, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(101, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(102, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(103, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(104, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(105, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(106, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(107, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(108, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(109, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(110, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(111, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(112, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(113, 9, '{"role":"3","roleText":"Employee","name":"Teste"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(114, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(115, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(116, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(117, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(118, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(119, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(120, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(121, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(122, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(123, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(124, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(125, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(126, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(127, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(128, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(129, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(130, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(131, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(132, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(133, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(134, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(135, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(136, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(137, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(138, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(139, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(140, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(141, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(142, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(143, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(144, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(145, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(146, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(147, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(148, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(149, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(150, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(151, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(152, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(153, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(154, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(155, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(156, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(157, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(158, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(159, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(160, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(161, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(162, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(163, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(164, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(165, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(166, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(167, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(168, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(169, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(170, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(171, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(172, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(173, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(174, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(175, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(176, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(177, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(178, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(179, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(180, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(181, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(182, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(183, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(184, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(185, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(186, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(187, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(188, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(189, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(190, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(191, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(192, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(193, 1, '{"role":"1","roleText":"System Administrator","name":"System Administrator"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(194, 18, '{"role":"1","roleText":"System Administrator","name":"asd"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(195, 18, '{"role":"1","roleText":"System Administrator","name":"asd"}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(196, 19, '{"role":"0","roleText":null,"name":null}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(197, 19, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(198, 19, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(199, 19, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 65.0.3325.181', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'Linux', '0000-00-00 00:00:00');
INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(200, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(201, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(202, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(203, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(204, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(205, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(206, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(207, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(208, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(209, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(210, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(211, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(212, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(213, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(214, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(215, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(216, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(217, 23, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(218, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(219, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(220, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(221, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(222, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(223, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(224, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(225, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(226, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(227, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(228, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(229, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(230, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(231, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(232, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(233, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(234, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(235, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(236, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(237, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(238, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(239, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(240, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(241, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(242, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(243, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(244, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(245, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(246, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(247, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(248, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(249, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(250, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(251, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(252, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(253, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(254, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(255, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(256, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(257, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(258, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(259, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(260, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(261, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(262, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(263, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(264, 32, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(265, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(266, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(267, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(268, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(269, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(270, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(271, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(272, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(273, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(274, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(275, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(276, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(277, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(278, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(279, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(280, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(281, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(282, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(283, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(284, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(285, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(286, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(287, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(288, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(289, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(290, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(291, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(292, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(293, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(294, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(295, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(296, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(297, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(298, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(299, 33, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(300, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(301, 24, '{"role":"0","name":null}', '172.18.0.1', 'Chrome 66.0.3359.117', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_reset_password`
--

CREATE TABLE IF NOT EXISTS `tbl_reset_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` bigint(20) NOT NULL DEFAULT '1',
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Extraindo dados da tabela `tbl_reset_password`
--

INSERT INTO `tbl_reset_password` (`id`, `email`, `activation_id`, `agent`, `client_ip`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'e1@email.com', 'JBKWRhfMbCXETPw', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:41:23', NULL, NULL),
(2, 'e1@email.com', 'q9D0b25GlxUCL6I', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:43:33', NULL, NULL),
(3, 'e1@email.com', 'YWcLOXeQPFhd9IZ', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:43:46', NULL, NULL),
(4, 'e1@email.com', 'TytH9rhzpl7EJZM', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:48:03', NULL, NULL),
(5, 'e1@email.com', 'CcmEHPX6SqGWIDr', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:48:45', NULL, NULL),
(6, 'e1@email.com', 'rfd0hC2q5jbRv4Y', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:49:03', NULL, NULL),
(7, 'e1@email.com', '73zYokOxmnXLgeF', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:49:32', NULL, NULL),
(8, 'e1@email.com', 'jFp52ZtWxv1lcIA', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:49:41', NULL, NULL),
(9, 'e1@email.com', 'Z8SV6FyjMHO2Q3d', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:49:52', NULL, NULL),
(10, 'e1@email.com', 'h0foJg4v3tWOa7P', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:50:13', NULL, NULL),
(11, 'e1@email.com', 'CJXPBrKntiwz4Sj', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:50:21', NULL, NULL),
(12, 'e1@email.com', 'iuQ4Z6ULh7Wvx5Y', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:50:41', NULL, NULL),
(13, 'e1@email.com', '2A6wVrzenhdG8k0', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:51:10', NULL, NULL),
(14, 'e1@email.com', 'fPoCtar9V5F6ylD', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:51:20', NULL, NULL),
(15, 'e1@email.com', '56NAwTeFjcio9SC', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:51:28', NULL, NULL),
(16, 'e1@email.com', '6gcWrbIBXePdUmL', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:52:37', NULL, NULL),
(17, 'e1@email.com', 'piGJE3u4MxzsKTd', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:53:38', NULL, NULL),
(18, 'e1@email.com', '5vz1LFGDsIp7Uh0', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:54:13', NULL, NULL),
(19, 'e1@email.com', 'uFUvKl2wmBiEdHO', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:55:14', NULL, NULL),
(20, 'e1@email.com', 'Xp9g6m5Gh1UYisr', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:55:22', NULL, NULL),
(21, 'e1@email.com', 'T9twHkg5eCoLFRW', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:56:03', NULL, NULL),
(22, 'e1@email.com', 'NJ3Px6EhdjwSBm7', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:56:30', NULL, NULL),
(23, 'e1@email.com', 'wqv5JPCX2NEldfZ', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:58:19', NULL, NULL),
(24, 'e1@email.com', 'IZ7BEqSYCLJtNpm', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:58:29', NULL, NULL),
(25, 'e1@email.com', 'OAc0yBmMP3irXpu', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:59:24', NULL, NULL),
(26, 'e1@email.com', '971a8bvetVQxdsO', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 07:59:40', NULL, NULL),
(27, 'e1@email.com', 'PtUSbauL4Tpq5MC', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:02:53', NULL, NULL),
(28, 'e1@email.com', 'QirDxLgY1yWjeb2', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:06:48', NULL, NULL),
(29, 'e1@email.com', 'MuRZ0nrc2gpEtVY', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:09:43', NULL, NULL),
(30, 'e1@email.com', 'VJMqtseXUfGHCYI', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:09:54', NULL, NULL),
(31, 'e1@email.com', '4jldPhZBI6yoFWe', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:10:51', NULL, NULL),
(32, 'e1@email.com', '0acFgPMVqbe12Ei', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:11:32', NULL, NULL),
(33, 'e1@email.com', '31IchaYHZMEWnTC', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:12:35', NULL, NULL),
(34, 'e1@email.com', 'iMKufBdqTghNSjL', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:14:52', NULL, NULL),
(35, 'e1@email.com', 'VHnyRFYePUxqhBv', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:15:20', NULL, NULL),
(36, 'e1@email.com', 'Z8SneF5twshYC1U', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:15:35', NULL, NULL),
(37, 'e1@email.com', 'oHe08zW3XVdFOx1', 'Chrome 65.0.3325.181', '172.18.0.1', 0, 1, '2018-04-14 08:15:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_roles`
--

CREATE TABLE IF NOT EXISTS `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'System Administrator'),
(2, 'Manager'),
(3, 'Employee');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Extraindo dados da tabela `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'gaditarobinho112@gmail.com', '$2y$10$Mu8/toYNPQxpLRxFcMPvqOTqYsrVSMXBFKkInbLsncrTlMjxS/1Pq', 'System Administrator', '9890098900', 1, 0, 0, '2015-07-01 18:56:49', 1, '2018-01-05 05:56:34'),
(2, 'manager@example.com', '$2y$10$quODe6vkNma30rcxbAHbYuKYAZQqUaflBgc4YpV9/90ywd.5Koklm', 'Manager', '9890098900', 2, 0, 1, '2016-12-09 17:49:56', 1, '2018-01-12 07:22:11'),
(3, 'employee@example.com', '$2y$10$UYsH1G7MkDg1cutOdgl2Q.ZbXjyX.CSjsdgQKvGzAgl60RXZxpB5u', 'Employee', '9890098900', 3, 0, 1, '2016-12-09 17:50:22', 3, '2018-01-04 07:58:28'),
(9, 'gaditarobinho121@gmail.com', '$2y$10$MvTxYw/dDVOUBBw5Tb5KS.9C6giW9amOjGzZWOPz1G66cdVSYYDPK', 'Teste', '1111111111', 3, 0, 1, '2018-04-14 07:23:54', 1, NULL),
(10, 'm1@email.com', '$2y$10$EqqY4uZG.OsfmacEIre5meDw5oF.YOhpeYEDKKOB3YY6ghdTXQypm', 'Teste2', '1111111111', 2, 0, 1, '2018-04-14 07:24:28', NULL, NULL),
(23, 'robsongomesdejesus2@hotmail.com', '$2y$10$etxBD418HbOWQCHkGWs/S.5BNDnvGHKoq5YkmXR3BMVQC98WajDV.', NULL, NULL, 0, 0, 0, '2018-04-20 06:45:17', NULL, '2018-04-20 06:45:17'),
(24, 'gaditarobinho12@gmail.com', '$2y$10$etxBD418HbOWQCHkGWs/S.5BNDnvGHKoq5YkmXR3BMVQC98WajDV.', NULL, NULL, 0, 0, 0, '2018-04-20 06:59:46', NULL, '2018-04-20 06:59:46'),
(25, 'robsongomesdejesus2@hotmail.com', '$2y$10$NYPVko4Oxc0IxrX3mntXq.EbzD3Mo2ZqlyqfertLgGVlaLnn7M.JW', NULL, NULL, 0, 0, 0, '2018-05-04 19:23:02', NULL, '2018-05-04 19:23:02'),
(33, 'robsongomesdejesus@hotmail.com', '$2y$10$9vrNZWZ7tNojkJ/scP0gIOptuSvIAUcTzetHIv5gsvJcgrTV3hDge', NULL, NULL, 0, 0, 0, '2018-05-04 19:37:31', NULL, '2018-05-04 19:37:31');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
