<?php 


function ci(){
    $ci = & get_instance();

    $ci->load->library('session');
    $userid = $ci->session->userdata('userId');
    if($userid){
        $ci->userid = $userid;
    }

  

    return $ci;
}

function getLang($mod){
    $ci = ci();
    $ci->load->library('modules');
    if(!isset($ci->_lang[$mod])){
        $ci->modules->lang($mod);
    }
}
function _lang($mod,$key,$data=false){
    $ci = ci();
    if(!isset($ci->_lang[$mod])){
        getLang($mod);
    }
    if(!isset($ci->_lang[$mod]) or !isset($ci->_lang[$mod][$key])){
        return "[_lang:$mod|$key]";
    }
    $text = $ci->_lang[$mod][$key];
    $text = vsprintf($text,$data);
  

    return $text;
}

function is_admin(){

    return true;
}


function has_permission($module,$type=null){
    return true;
}

function getAddresses($domain) {
    $records = dns_get_record($domain);
    $res = array();
    foreach ($records as $r) {
      if ($r['host'] != $domain) continue; // glue entry
      if (!isset($r['type'])) continue; // DNSSec
  
      if ($r['type'] == 'A') $res[] = $r['ip'];
      if ($r['type'] == 'AAAA') $res[] = $r['ipv6'];
    }
    return $res;
  }
  
  function getAddresses_www($domain) {
    $res = getAddresses($domain);
    if (count($res) == 0) {
      $res = getAddresses('www.' . $domain);
    }
    return $res;
  }


  function ago($date){

    $ci = ci();
    if(!isset($ci->modules)){
        $ci->load->library('modules');
    }
    
    return $ci->modules->load['ago']->ago($date);
  }