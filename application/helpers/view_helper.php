<?php 
function get_theme(){
    if(isset($_GET['theme'])){
        $_SESSION['theme'] = $_GET['theme'];
    }
    if(!isset($_SESSION['theme'])){
        $_SESSION['theme'] = 'vali-admin';
    }
    return 'default';
}
function view($view,$data,$output=false){
    $ci = &get_instance();
    $ci->view = $view;
    if(isset($data['struct'])){
        extract($data['struct']);
    }

    $_view = $view;
  

    $_view = explode('/',$_view);
    $struct = false;
        if(isset($_view[0])){
            $base = array_shift($_view);
            $file = APPPATH.'/views/'.get_theme().'/struct/'.$base.'.php';
        
            if(file_exists($file)){
                ob_start();
                    $view = implode('/',$_view);
                    include_once $file;
                    $struct = ob_get_contents();
                ob_end_clean();
            }
            
        }

        if(!isset($_contents_)):
            ob_start();
                $core = &get_instance();
                $data['view'] = $view;
                $data['data'] = $data;
                $view = $path = get_theme().'/'.$view;
                $core->load->view($view,$data,$output);
                $_contents_ = ob_get_contents();
            ob_end_clean();   
        endif;

        if($struct)
            echo str_replace('{[content]}',$_contents_,$struct);
        if(!$struct)
            echo $_contents_;
       
}
function view_struct($view,$data,$output){
    $core = &get_instance();

    $data['view'] = $view;
    $data['data'] = $data;
    $view = $path = get_theme().'/'.$view;
    return $core->load->view($view,$data,true);
    
}
function module_view($view,$data=[],$output=false){
    $core = &get_instance();
    
    $seg = $core->uri->segments;
   
    $base = isset($seg[1])?$seg[1].'/':'';
   
    $data['view'] = $view;

    $exp_view = explode('/',$view);
    $module = array_shift($exp_view);
    $module_file =$module.'/{base}'.implode('/',$exp_view).'.php'; 
    

    $path = get_theme().'/'.$base;

    $_view = $view;
    $view = $path.$view;
    $file_view = APPPATH.'views/'.$view.'.php';

        
    $base_theme = APPPATH.'views/'.get_theme().'/modules/'.str_replace('{base}',$base,$module_file);
    $base_module = APPPATH.'libraries/modules/'.str_replace('{base}',$base,$module_file);
    $module_view = APPPATH.'libraries/modules/'.str_replace('{base}','',$module_file);
    $base_router = APPPATH.'views/'.$view.'.php';
    $base_view = APPPATH.'views/'.get_theme().'/'.$_view.'.php';

    $res_view = 'error';
    if(file_exists($base_theme)){
        $view = get_theme().'/modules/'.str_replace('{base}',$base,$module_file);
        $res_view =$core->load->view($view,$data,$output);
    }elseif(file_exists($base_router)){
        $res_view =$core->load->view($view,$data,$output);
    }elseif(file_exists($module_view)){
        $view = '../libraries/modules/'.str_replace('{base}','',$module_file);
        $res_view = $core->load->view($view,$data,$output);
    }elseif(file_exists($base_module)){
   
        $view = '../libraries/modules/'.str_replace('{base}',$base,$module_file);
        $res_view =$core->load->view($view,$data,$output);
    }elseif(file_exists($base_view)){
        
        $res_view = $core->load->view(get_theme().'/'.$_view,$data,$output);
    }
    
    if($output == true){
        return $res_view;
    }
}

function load_views_mod($module,$position,$output=false){
    
    $ci = & get_instance();
    $arr = [];
    $return = '';
    foreach($ci->modules->load as $mod => $values){

        $path = APPPATH."views/".get_theme()."/modules/$mod/views/$module/*$position.php";
        $path = glob($path);
        foreach ($path as $key => $value) {
            # code..
            $value = str_replace(APPPATH,'../',$value);
            $return = $ci->load->view($value,[],$output);
            $file_arr = str_replace('../views/'.get_theme(),'',$value);
            $arr[] =$file_arr; 
        }

    }
    
    foreach($ci->modules->load as $mod => $values){

       $path = APPPATH."libraries/modules/$mod/views/$module/*$position.php";
       
        $path = glob($path);
        foreach ($path as $key => $value) {
            # code..

            $file_arr = str_replace(APPPATH."libraries",'',$value);
            if(!in_array($file_arr,$arr)){
                $value = str_replace(APPPATH,'../',$value);
                $return = $ci->load->view($value,[],$output);
            }
        }
    }
    if($output == true){
        return $return;
    }
}