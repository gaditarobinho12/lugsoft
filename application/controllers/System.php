<?php 

class System extends CI_Controller
{


    function index(){
       
        $seg = $this->uri->segments;
      
        if(!isset($seg[1])){
            $this->load->view(get_theme().'/index');
        }else{
            $this->call($seg[1]);
        }
      
    }

    function __call($method,$args){

        $seg = $this->uri->segments;

        if(isset($this->modules->load[$args[0]])){
        
            if(!isset($seg[2])){

                $this->modules->load[$args[0]]->view();
              
            }else{
                $this->modules->load[$args[0]]->{$seg[2]}();
            }
        }
     
    }
}
