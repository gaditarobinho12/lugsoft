


    </div><!-- /.login-box -->

<script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
      <script>
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function checkCookie() {
            var user = getCookie("username");
            if (user != "") {
                alert("Welcome again " + user);
            } else {
                user = prompt("Please enter your name:", "");
                if (user != "" && user != null) {
                    setCookie("username", user, 365);
                }
            }
        }
        $(function(){

          if(getCookie('input_remember') != ''){
           
            $('input[name=remember]').attr('checked',1);
          }else{

           
          } 
          $('input[name=email]').val(decodeURIComponent(getCookie('input_email')));
          $('input[name=password]').val(decodeURIComponent(getCookie('input_password')));

          if(getCookie('input_email')=='' && getCookie('input_remember') != ''){

            $('input[name=email]').val(decodeURIComponent(getCookie('remember_email')));
            $('input[name=password]').val(decodeURIComponent(getCookie('remember_password')));    
            
          }

        
        });
        </script>

</body>
</html>