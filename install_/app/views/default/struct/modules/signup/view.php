

      <div class="login-box-body">
        <p class="login-box-msg"><?php echo $lang['title_text']; ?></p>
        <?php $ci->load->helper('form'); ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
            </div>
        </div>
        <?php
        $ci->load->helper('form');
        $error = $ci->session->flashdata('error');
        if($error)
        {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $error; ?>                    
            </div>
        <?php }
        $success = $ci->session->flashdata('success');
        if($success)
        {
            ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $success; ?>                    
            </div>
        <?php } ?>
        
        <form action="" method="POST">
          <div class="form-group has-feedback">
            <input type="email" class="form-control email" placeholder="Email" name="email" required />
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>


          <div class="row">
            <div class="col-xs-8">    
              <!-- <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>  -->                       
            </div><!-- /.col -->
            
            <?php load_views_mod('login','form_top');?>
          

            <div class="col-xs-4">
              <input type="submit" name="post_signup" class="btn btn-primary btn-block btn-flat" value="Sign In" />
            </div><!-- /.col -->
          </div>
       
       </form>

        <hr >
        <a href="<?php echo base_url() ?>">Home</a>
        |
        <a href="<?php echo base_url() ?>/loginMe">Login</a>
        |
        <a href="<?php echo base_url() ?>forgotPassword">Forgot Password</a>
        
      </div><!-- /.login-box-body -->

