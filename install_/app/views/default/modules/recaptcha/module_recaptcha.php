<?php 

class module_recaptcha {
    
    function __construct(){
        $this->about = [
            'code'=>'recaptcha',
            'name'=> 'Recaptcha Google',
            'author'=>'Robson Gomes',
            'email_author'=>'gaditarobinho12@gmail.com',
            'version'=>'1.0',
            'status'=>10
        ];
        $this->ci = & get_instance();
    }

    function on(){
      
        return [
            'login'=>[
                'Validate'=>function($data){
                    $this->Validate($data);
                }
            ],'signup'=>[
                'Validate'=>function($data){
                    $this->Validate($data);
                }
            ]
            ];
    }
    function Validate($data){

        exit;
        $recaptchaResponse = $this->ci->input->post('g-recaptcha-response');
        $secret = '6LcGJFMUAAAAAM7aaxQ3-TpXfaSjYk725ugs15Qw';
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data1 = array('secret' => $secret, 'response' => $recaptchaResponse);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($ch);
        curl_close($ch);
        $status = json_decode($response, true);

        if ($status['success']) {

        }else{
            $this->ci->session->set_flashdata('error', 'Error recaptcha');
      
            redirect('/loginMe');
            
        }
    }

}